<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Laravel\Socialite\Facades\Socialite;

class AzureAdController extends Controller
{
    /**
     * 
     */
    public function handleLoginRedirect() {
        return Socialite::driver('azure')->redirect();
    }

    /**
     * 
     */
    public function handleCallback( Request $request ) {
        if ( ! $request->has('code') ) {
            return redirect()->route('login');
        }

        try {
            $user = Socialite::driver('azure')->user();
            dd($user);
        } catch ( Exception $ex) {
            dd($ex);
        }
    }

}
